<?php
/**
 * Template Name: Unsplash Credits
 *
 * @package     WP-Blueprint
 * @since       2.0
 */

get_header();
?>
<main class="wp-blueprint-main">
	<h1>Unsplash Credits</h1>
	<!-- Nametags -->
	<span class=blueprint-footer__link>Photo by <a href="https://unsplash.com/@jontyson?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Jon Tyson</a> on <a href="https://unsplash.com/s/photos/resume?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a></span>
	<!-- Resume -->
	<span class=blueprint-footer__link>Photo by <a href="https://unsplash.com/@helloquence?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Helloquence</a> on <a href="https://unsplash.com/s/photos/resume?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a></span>
	<!-- Toolbox -->
	<span class=blueprint-footer__link>Photo by <a href="https://unsplash.com/@barnimages?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Barn Images</a> on <a href="https://unsplash.com/s/photos/toolbox?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a></span>
	<!-- Connect -->
	<span class=blueprint-footer__link>Photo by <a href="https://unsplash.com/@annadziubinska?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Anna Dziubinska</a> on <a href="https://unsplash.com/s/photos/busy-train-station?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a></span>
</main>
<?php
get_footer();
