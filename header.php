<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "content" main element.
 *
 * @package WP-Blueprint
 * @since   2.0
 */

/**
 * Return the title of the current page
 *
 * @return  string
 */
function wp_blueprint_site_title() {
	if ( is_front_page() ) {
		return get_bloginfo( 'name' );
	} else {
		return wp_title();
	}
}
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="<?php echo esc_attr( is_admin_bar_showing() ? 'html-with-admin-bar' : 'html-no-admin-bar' ); ?>">
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="theme-color" content="#fafafa">
		<title><?php echo esc_html( wp_blueprint_site_title() ); ?></title>
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
		<link rel="shortcut icon" href="<?php echo sprintf( '%s/favicon.png', esc_url( get_template_directory_uri() ) ); ?>" />
		<?php wp_head(); ?>
	</head>
<body <?php body_class(); ?>>
	<header role="banner" class="blueprint-header">
		<div class="blueprint-header__content">
			<a href="<?php echo esc_url( home_url() ); ?>">
				<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="mobile-alt" class="blueprint-header__icon svg-inline--fa fa-mobile-alt fa-w-10" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512">
					<path fill="#3da5d9" d="M272 0H48C21.5 0 0 21.5 0 48v416c0 26.5 21.5 48 48 48h224c26.5 0 48-21.5 48-48V48c0-26.5-21.5-48-48-48zM160 480c-17.7 0-32-14.3-32-32s14.3-32 32-32 32 14.3 32 32-14.3 32-32 32zm112-108c0 6.6-5.4 12-12 12H60c-6.6 0-12-5.4-12-12V60c0-6.6 5.4-12 12-12h200c6.6 0 12 5.4 12 12v312z"></path>
				</svg>
			</a>
			<a href="<?php echo esc_url( home_url() ); ?>">
				<div class="blueprint-header__titles-wrapper">
					<span class="blueprint-header__title">Caleb Young</span>
					<span class="blueprint-header__subtitle">Web, Built from the Ground Up</span>
				</div>
			</a>
			<div class="blueprint-header__desktop-nav">
				<a href="<?php echo esc_url( sprintf( '%s/portfolio', home_url() ) ); ?>" class="blueprint-header__nav-link">Portfolio</a>
				<a href="<?php echo esc_url( sprintf( '%s/blog', home_url() ) ); ?>" class="blueprint-header__nav-link">Blog</a>
			</div>
			<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="search" class="blueprint-header__search svg-inline--fa fa-search fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
				<path fill="#364652" d="M505 442.7L405.3 343c-4.5-4.5-10.6-7-17-7H372c27.6-35.3 44-79.7 44-128C416 93.1 322.9 0 208 0S0 93.1 0 208s93.1 208 208 208c48.3 0 92.7-16.4 128-44v16.3c0 6.4 2.5 12.5 7 17l99.7 99.7c9.4 9.4 24.6 9.4 33.9 0l28.3-28.3c9.4-9.4 9.4-24.6.1-34zM208 336c-70.7 0-128-57.2-128-128 0-70.7 57.2-128 128-128 70.7 0 128 57.2 128 128 0 70.7-57.2 128-128 128z"></path>
			</svg>
		</a>
	</header>
	<div class="blueprint-mobile-pill">
		<i class="far fa-compass"></i>
	</div>
	<div class="blueprint-content">
