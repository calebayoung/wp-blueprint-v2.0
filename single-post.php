<?php
/**
 * The auto-generated single blog post page
 *
 * @package     WP-Blueprint
 * @since       2.0
 */

get_header();
?>
<main class="wp-blueprint-main">
	<h1><?php the_title(); ?></h1>
</main>
<?php
get_footer();
