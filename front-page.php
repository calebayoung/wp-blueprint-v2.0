<?php
/**
 * The auto-generated template for the front-page.
 *
 * @package WP-Blueprint
 * @since   2.0
 */

get_header();

$args         = array(
	'orderby'        => 'publish_date',
	'order'          => 'DESC',
	'posts_per_page' => 2,
	'cat'            => '-5',
);
$query = new WP_Query( $args );
$recent_posts = $query->posts;

if ( have_posts() ) {
	while ( have_posts() ) {
		the_post();
		$content = get_the_content();
		?>
		<div class="hero">
			<div class="hero__content">
				<h1 class="hero__text">Web development,</h1>
				<h1 class="hero__text">built from the ground up</h1>
			</div>
			<button class="hero__button button button--primary">Learn More</button>
			<div class="hero__overlay"></div>
			<div class="hero__image-wrapper">
				<img class="hero__image" src="<?php echo esc_url( sprintf( '%s/images/front-page-hero.jpg', get_template_directory_uri() ) ); ?>">
			</div>
		</div>
		<div class="front-page-card-container">
			<div>
				<a href="#about-me">
					<div class="card">
						<div class="card__overlay"></div>
						<picture class="card__picture">
							<img class="card__image" src="<?php echo esc_url( sprintf( '%s/images/nametags.jpg', get_template_directory_uri() ) ); ?>">
						</picture>
						<h2 class="card__title">About Me</h2>
						<p class="card__text"><?php echo esc_html( strip_tags( $content ) ); ?></p>
					</div>
				</a>
			</div>
			<div>
				<a href="<?php echo esc_url( sprintf( '%s/resume', home_url() ) ); ?>">
					<div class="card">
						<div class="card__overlay"></div>
						<picture class="card__picture">
							<img class="card__image" src="<?php echo esc_url( sprintf( '%s/images/resume.jpg', get_template_directory_uri() ) ); ?>">
						</picture>
						<h2 class="card__title">Resumé</h2>
						<p class="card__text">Past work experience, skills, education, and contact information.</p>
					</div>
				</a>
			</div>
			<div>
				<a href="<?php echo esc_url( sprintf( '%s/technology-toolbox', home_url() ) ); ?>">
					<div class="card">
						<div class="card__overlay"></div>
						<picture class="card__picture">
							<img class="card__image" src="<?php echo esc_url( sprintf( '%s/images/toolbox.jpg', get_template_directory_uri() ) ); ?>">
						</picture>
						<h2 class="card__title">Technology Toolbox</h2>
						<p class="card__text">View a collection of tools, technology, and frameworks I am familiar with and/or have used in the past.</p>
					</div>
				</a>
			</div>
			<div>
				<a href="<?php echo esc_url( sprintf( '%s/connect-with-me', home_url() ) ); ?>">
					<div class="card">
						<div class="card__overlay"></div>
						<picture class="card__picture">
							<img class="card__image" src="<?php echo esc_url( sprintf( '%s/images/connect.jpg', get_template_directory_uri() ) ); ?>">
						</picture>
						<h2 class="card__title">Connect with Me</h2>
						<p class="card__text">The internet is a big place! Find links to my accounts on your favorite social media or development platform.</p>
					</div>
				</a>
			</div>
			<?php
			$card_image_class = array(
				'class' => 'card__image',
			);
			foreach ( $recent_posts as $recent_post ) {
				?>
				<div>
					<a href="<?php echo esc_url( get_permalink( $recent_post->ID ) ); ?>">
						<div class="card">
							<div class="card__overlay"></div>
							<picture class="card__picture">
								<?php
								echo get_the_post_thumbnail( $recent_post->ID, 'medium', $card_image_class );
								?>
							</picture>
							<h2 class="card__title"><?php echo esc_html( $recent_post->post_title ); ?></h2>
							<p class="card__text"><?php echo esc_html( get_the_excerpt( $recent_post->ID ) ); ?></p>
							<p class="card__label card__label--secondary">Recent Blog Post</p>
						</div>
					</a>
				</div>
				<?php
			}
			?>
		</div>
		<main id="about-me" class="wp-blueprint-main">
			<h3 id="bio-header">Hey there!</h3>
			<img id="bio-photo" src="<?php echo esc_url( sprintf( '%s/images/bio-photo.jpg', get_template_directory_uri() ) ); ?>">
			<?php the_content(); ?>
		</main>
		<?php
	}
}

get_footer();
