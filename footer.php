<?php
/**
 * The template for displaying the footer.
 *
 * @package WP-Blueprint
 * @since   2.0
 */

?>
		</div>
		<div class="blueprint-footer">
			<div class="blueprint-footer__section">
				<span class="blueprint-footer__title">Sitemap</span>
				<a href="<?php echo esc_url( home_url() ); ?>" class="blueprint-footer__link grey-link">Home</a>
				<a href="<?php echo esc_url( sprintf( '%s/portfolio', home_url() ) ); ?>" class="blueprint-footer__link grey-link">Portfolio</a>
				<a href="<?php echo esc_url( sprintf( '%s/blog', home_url() ) ); ?>" class="blueprint-footer__link grey-link">Blog</a>
			</div>
			<div class="blueprint-footer__section">
				<span class="blueprint-footer__title">Social</span>
				<div class="blueprint-footer__social">
					<i class="blueprint-footer__social-icon fab fa-facebook"></i>
					<i class="blueprint-footer__social-icon fab fa-instagram"></i>
					<i class="blueprint-footer__social-icon fab fa-twitter"></i>
					<i class="blueprint-footer__social-icon fab fa-linkedin"></i>
					<i class="blueprint-footer__social-icon fab fa-github"></i>
					<i class="blueprint-footer__social-icon fab fa-gitlab"></i>
					<i class="blueprint-footer__social-icon fab fa-medium"></i>
				</div>
			</div>
			<div class="blueprint-footer__section">
				<span class="blueprint-footer__title">Acknowledgements</span>
				<span class="blueprint-footer__link">Icons provided by <a href="">FontAwesome</a></span>
				<span class="blueprint-footer__link">Photos provided by Unsplash - <a href="<?php echo esc_url( sprintf( '%s/unsplash-credits', home_url() ) ); ?>">Credits</a></span>
				<span class="blueprint-footer__link">This site is proudly powered by <a href="">WordPress</a></span>
			</div>
		</div>
		<?php
		wp_footer();
		?>
	</body>
</html>
