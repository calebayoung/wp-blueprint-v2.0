<?php
/**
 * WP-Blueprint functions and definitions
 *
 * @package     WP-Blueprint
 * @since       2.0
 */

/**
 * Register all styles and scripts
 */
function wp_blueprint_register_scripts() {
	$theme_directory_url  = get_template_directory_uri();
	$theme_directory_path = get_template_directory();
	wp_register_style( 'wp-blueprint', "$theme_directory_url/style.css", array(), filemtime( "$theme_directory_path/style.css" ) );
	wp_register_style( 'wp-blueprint-header', "$theme_directory_url/css/header.css", array(), filemtime( "$theme_directory_path/css/header.css" ) );
	wp_register_style( 'wp-blueprint-footer', "$theme_directory_url/css/footer.css", array( 'wp-blueprint' ), filemtime( "$theme_directory_path/css/footer.css" ) );
	wp_register_style( 'front-page', "$theme_directory_url/css/front-page.css", array( 'wp-blueprint' ), filemtime( "$theme_directory_path/css/front-page.css" ) );
	wp_register_style( 'normalize', "$theme_directory_url/css/normalize.css", array(), filemtime( "$theme_directory_path/css/normalize.css" ) );
	wp_register_style( 'google-fonts', 'https://fonts.googleapis.com/css?family=Alice|Rubik&display=swap', false, 'all' );
	wp_register_script( 'font-awesome', 'https://kit.fontawesome.com/71e7818d2f.js', array(), true, false );
}
add_action( 'wp_enqueue_scripts', 'wp_blueprint_register_scripts' );

/**
 * Enqueue styles and scripts based on the current page
 */
function wp_blueprint_enqueue_scripts() {
	wp_enqueue_style( 'wp-blueprint' );
	wp_enqueue_style( 'wp-blueprint-header' );
	wp_enqueue_style( 'wp-blueprint-footer' );
	wp_enqueue_style( 'normalize' );
	wp_enqueue_style( 'google-fonts' );
	wp_enqueue_script( 'font-awesome' );
	if ( is_front_page() ) {
		wp_enqueue_style( 'front-page' );
	}
}
add_action( 'wp_enqueue_scripts', 'wp_blueprint_enqueue_scripts' );

/**
 * Customize 'excerpt more' string
 */
function custom_excerpt_more() {
	return ' . . .';
}
add_filter( 'excerpt_more', 'custom_excerpt_more' );

/**
 * Enable post thumnails
 */
add_theme_support( 'post-thumbnails' );
