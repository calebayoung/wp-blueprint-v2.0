<?php
/**
 * The auto-generated blog posts page
 *
 * @author      Caleb Young <caleb.a.young@gmail.com>
 * @package     WP-Blueprint
 * @since       2.0
 */

get_header();
?>
<main class="wp-blueprint-main">
	<h1>Blog</h1>
</main>
<?php
get_footer();
